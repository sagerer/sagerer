Hello, I am Florian Sagerer and this is my GitLab profile.
Here is the code of [all my personal projects](https://gitlab.com/users/sagerer/projects) hosted.

- [My website](https://floriansagerer.de/)
- [My e-mail](mailto:f.sagerer@protonmail.com)
- [My projects](https://gitlab.com/users/sagerer/projects)
